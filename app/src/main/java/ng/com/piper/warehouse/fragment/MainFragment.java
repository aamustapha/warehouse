package ng.com.piper.warehouse.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.utilities.Constants;
import ng.com.piper.warehouse.utilities.FormWrapper;
import ng.com.piper.warehouse.utilities.Preferences;


public class MainFragment extends Fragment {

    private View view;
    private Uri currentImage;
    boolean editable;
    Preferences pref ;

    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance(Object... vars) {

        Bundle args = new Bundle();
        if(vars.length > 0)
            args.putBoolean("editable", (Boolean) vars[0]);

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editable = getArguments().getBoolean("editable", true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signup, container, false);
    }


    @Override
    public void onViewCreated(View vw, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = vw;
        pref = Preferences.getInstance(view.getContext());
        view.findViewById(R.id.company_logo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent imagePicker= new Intent();
                imagePicker.setAction(Intent.ACTION_GET_CONTENT);
                imagePicker.setType("image/*");

                startActivityForResult(imagePicker, 0);
            }
        });
        final FormWrapper wrap = new FormWrapper(view);
        final Fragment frag;
        if (pref.has("company_name"))
            frag = SigninFragment.newInstance(Constants.HOME);
        else
            frag = SetPasswordFragment.newInstance(Constants.HOME);


        view.findViewById(R.id.btn_proceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.set("company_logo", currentImage.toString());
                pref.set("company_name", wrap.getEditTextValue(R.id.company_name));
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                        .replace(R.id.main_container, frag)
                        .addToBackStack("a")
                        .commit();
            }

        });
        if(pref.has("company_name")) {
            wrap.injectEditText(R.id.company_name, pref.get("company_name"));
            wrap.disableEditText(R.id.company_name);
        }

        if(pref.has("company_logo")){
           currentImage = Uri.parse(pref.get("company_logo"));
            setImage();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            currentImage = data.getData();
            setImage();
        }
    }

    public void setImage(){

        Log.e("A", currentImage.getPath());
        ((ImageView)view.findViewById(R.id.company_logo)).setImageURI(currentImage);
    }
}
