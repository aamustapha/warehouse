package ng.com.piper.warehouse.databases;

import com.orm.SugarRecord;

import java.util.Date;

/**
 * Created by amustapha on 9/26/17.
 */

public class Sale extends SugarRecord {
    public String customer, phone;
    Date date;

    public Sale(){}
    public Sale(String customer, String phone, Date date) {
        this.customer = customer;
        this.phone = phone;
        this.date = date;
    }

    public String getDate(){
        return String.valueOf(date);
    }

}
