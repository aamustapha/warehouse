package ng.com.piper.warehouse.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.adapters.SoldItemAdapter;
import ng.com.piper.warehouse.databases.Sold;


/**
 * Created by amustapha on 9/26/17.
 */

public class SaleFragment extends DialogFragment {
    long id;
    public SaleFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        id = getArguments().getLong("id");
        return inflater.inflate(R.layout.fragment_sold, container);
    }

    public static SaleFragment newInstance(long id) {
        SaleFragment frag = new SaleFragment();
        Bundle args = new Bundle();
        args.putLong("id", id);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        RecyclerView recycler = view.findViewById(R.id.item_list);
        recycler.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(new SoldItemAdapter(Sold.find(Sold.class, " sale = ?", id+""), getFragmentManager()));
    }

}
