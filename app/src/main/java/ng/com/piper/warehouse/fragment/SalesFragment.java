package ng.com.piper.warehouse.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import ng.com.piper.warehouse.HomeActivity;
import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.adapters.SaleAdapter;
import ng.com.piper.warehouse.databases.Sale;


public class SalesFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;

    public SalesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = view.findViewById(R.id.item_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(), LinearLayout.VERTICAL));
        recyclerView.setAdapter(new SaleAdapter(Sale.listAll(Sale.class), getFragmentManager()));
    }

    public static SalesFragment newInstance(Object... vars) {

        Bundle args = new Bundle();
        SalesFragment fragment = new SalesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycler, container, false);

    }


    @Override
    public void onStart() {
        super.onStart();
        ((HomeActivity) getActivity()).showFab(R.drawable.ic_shopping_basket_black_24dp, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SellFragment frag = SellFragment.newInstance();
                frag.show(getFragmentManager(), "item");
            }
        });

    }

}
