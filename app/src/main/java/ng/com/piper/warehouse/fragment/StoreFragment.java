package ng.com.piper.warehouse.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.common.collect.Lists;

import ng.com.piper.warehouse.HomeActivity;
import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.adapters.ItemAdapter;
import ng.com.piper.warehouse.databases.Item;


public class StoreFragment extends Fragment {
    View view;
    public StoreFragment() {
        // Required empty public constructor
    }

    public static StoreFragment newInstance(Object... vars) {

        Bundle args = new Bundle();
        StoreFragment fragment = new StoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recycler, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        load();

    }

    public void load(){
        RecyclerView item_list = view.findViewById(R.id.item_list);
        LinearLayoutManager mng = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);
        item_list.setLayoutManager(mng);
        item_list.addItemDecoration(new DividerItemDecoration(view.getContext(), LinearLayout.VERTICAL));
        item_list.setItemAnimator(new DefaultItemAnimator());

        item_list.setAdapter(new ItemAdapter(Lists.newArrayList(Item.findAll(Item.class)), getFragmentManager()));

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity) getActivity()).showFab(R.drawable.ic_truck, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ItemFragment frag = ItemFragment.newInstance(0);
                frag.show(getFragmentManager(), "item");
            }
        });
    }
}
