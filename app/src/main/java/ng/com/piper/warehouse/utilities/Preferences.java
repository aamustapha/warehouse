package ng.com.piper.warehouse.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by amustapha on 10/6/17.
 */

public class Preferences {
    private SharedPreferences session;
    public Preferences(Context context) {
        session = context.getSharedPreferences("warehouse", Context.MODE_PRIVATE);
    }

    public static Preferences getInstance(Context context) {
        return new Preferences(context);
    }

    public boolean set(String key, String value){
        return session.edit().putString(key, value).commit();
    }

    public String get(String key){
        return session.getString(key, "default");
    }

    public boolean has(String key){
        return session.contains(key);
    }
}
