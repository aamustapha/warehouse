package ng.com.piper.warehouse.databases;

import com.orm.SugarRecord;

/**
 * Created by amustapha on 9/26/17.
 */


public class Item extends SugarRecord {
    public String name;
    public int availability, price;

    public Item(){}
    public Item(String name, int availability, int price) {
        this.name = name;
        this.availability = availability;
        this.price = price;
    }

    public Item setName(String name) {
        this.name = name;
        return this;
    }

    public Item setAvailability(int availability) {
        this.availability = availability;
        return this;
    }

    public Item setPrice(int price) {
        this.price = price;
        return this;
    }
}
