package ng.com.piper.warehouse.databases;

import com.orm.SugarRecord;

/**
 * Created by amustapha on 9/28/17.
 */

public class Staff extends SugarRecord {
    public String name, rank, phone;

    public Staff(){}

    public Staff(String name, String rank, String phone) {
        this.name = name;
        this.rank = rank;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getRank() {
        return rank;
    }

    public String getPhone() {
        return phone;
    }

    public Staff setName(String name) {
        this.name = name;
        return this;
    }

    public Staff setRank(String rank) {
        this.rank = rank;
        return this;
    }

    public Staff setPhone(String phone) {
        this.phone = phone;
        return this;
    }
}
