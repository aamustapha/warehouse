package ng.com.piper.warehouse.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.databases.Staff;
import ng.com.piper.warehouse.utilities.FormWrapper;


/**
 * Created by amustapha on 9/26/17.
 */

public class StaffFragment extends DialogFragment {
    public StaffFragment() {
//
    }
    private long id = 0;
    private Staff staff;
    FormWrapper wrapper;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_staff
                , container);
    }

    public static StaffFragment newInstance(long id) {
        StaffFragment frag = new StaffFragment();
        Bundle args = new Bundle();
        args.putLong("id", id);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        id = getArguments().getLong("id", 0);
        wrapper = new FormWrapper(view);
        init(view);

        view.findViewById(R.id.btn_commit_staff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                staff.setName(wrapper.getEditTextValue(R.id.staff_name))
                        .setRank(wrapper.getEditTextValue(R.id.staff_rank))
                        .setPhone(wrapper.getEditTextValue(R.id.staff_phone))
                        .save();
                Toast.makeText(getDialog().getContext(), "Staff has been successfully saved", Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }
        });

        view.findViewById(R.id.btn_call_staff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call(staff.getPhone());
            }
        });

        view.findViewById(R.id.btn_message_staff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                message(staff.getPhone());
            }
        });
        view.findViewById(R.id.btn_delete_staff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(view.getContext())
                        .setTitle("Confirm delete")
                        .setIcon(R.drawable.ic_delete_sweep_black_24dp)
                        .setMessage("Are you sure you really want to delete this staff? This action cannot be reversed")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                staff.delete();
                                Toast.makeText(getDialog().getOwnerActivity(), "Staff has been successfully removed", Toast.LENGTH_SHORT).show();
                                getDialog().dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //abort
                            }
                        })
                        .show();

            }
        });

        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void init(View view){
        if (id > 0){
            staff = Staff.findById(Staff.class, id);
            wrapper.injectEditText(R.id.staff_name, staff.getName());
            wrapper.injectEditText(R.id.staff_rank, staff.getRank());
            wrapper.injectEditText(R.id.staff_phone, staff.getPhone());

            wrapper.injectTextView(R.id.btn_commit_staff, "Save");
        }else{
            staff = new Staff();
            view.findViewById(R.id.actions).setVisibility(View.GONE);
        }
    }

    public void message(String phone){
        String uri = "smsto:" + phone;
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(uri));
        intent.putExtra("compose_mode", true);
        startActivity(intent);
    }

    public void call(String phone){
        Uri number = Uri.parse("tel:" + phone);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);

    }

}
