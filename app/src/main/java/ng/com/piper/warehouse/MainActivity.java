package ng.com.piper.warehouse;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ng.com.piper.warehouse.fragment.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_container, MainFragment.newInstance())
                .commit();

    }


}
