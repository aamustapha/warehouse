package ng.com.piper.warehouse.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.customviews.lato.TextViewLato;
import ng.com.piper.warehouse.customviews.lato.TextViewLatoBold;
import ng.com.piper.warehouse.databases.Item;
import ng.com.piper.warehouse.fragment.ItemFragment;

/**
 * Created by amustapha on 9/26/17.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    List<Item> items;
    FragmentManager fm;

    public ItemAdapter(List<Item> items, FragmentManager fm) {
        this.items = items;
        this.fm = fm;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.init(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextViewLatoBold name;
        TextViewLato price, availability;
        ImageView edit;
        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.item_name);
            price = itemView.findViewById(R.id.item_price);
            availability = itemView.findViewById(R.id.item_availability);
            edit = (ImageView) itemView.findViewById(R.id.edit_item);
        }

        public void init(final Item item){
            name.setText(item.name);
            price.setText("₦"+item.price);
            availability.setText(item.availability + " remaining");

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ItemFragment frag = ItemFragment.newInstance(item.getId());
                    frag.show(fm, "delete");
                }
            });
        }


    }
}
