package ng.com.piper.warehouse.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.adapters.SellAdapter;
import ng.com.piper.warehouse.databases.Item;
import ng.com.piper.warehouse.databases.Sale;
import ng.com.piper.warehouse.databases.Sold;
import ng.com.piper.warehouse.utilities.FormWrapper;


/**
 * Created by amustapha on 9/26/17.
 */

public class SellFragment extends DialogFragment {

    FormWrapper wrapper;
    Sale sale = null;

    public SellFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sell, container);
    }

    public static SellFragment newInstance(Object... arg) {
        SellFragment frag = new SellFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        wrapper = new FormWrapper(view);

        final Spinner spin = view.findViewById(R.id.item);
        List<Map<String, String>> items = new ArrayList<>();
        for(Item item : Lists.newArrayList(Item.findAll(Item.class))){
            Map<String, String> hash = new HashMap<>();
            hash.put("name", item.name);
            hash.put("id", item.getId() + "");
            items.add(hash);
        }
        final SimpleAdapter adapter = new SimpleAdapter(view.getContext(), items, android.R.layout.simple_list_item_1, new String[]{"name"}, new int[]{android.R.id.text1});
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spin.setAdapter(adapter);

        final RecyclerView recyler = view.findViewById(R.id.item_list);
        recyler.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));

        view.findViewById(R.id.btn_add_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sale == null) {
                    sale = new Sale(wrapper.getEditTextValue(R.id.buyer), wrapper.getEditTextValue(R.id.phone), new Date());
                    sale.save();
                }

                Map<String, String> hash = (Map<String, String>) spin.getSelectedItem();
                Item item = Item.findById(Item.class, Long.parseLong(hash.get("id")));
                Sold sold = new Sold(sale, item, Integer.parseInt(wrapper.getEditTextValue(R.id.quantity)));
                sold.save();

                item.availability -= Integer.parseInt(wrapper.getEditTextValue(R.id.quantity));
                item.save();

                List<Sold> sold_list = Lists.newArrayList(Sold.find(Sold.class, " sale = ? ", sale.getId()+""));
                recyler.setAdapter(new SellAdapter(sold_list, sale.getId()));

            }
        });

        view.findViewById(R.id.btn_commit_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        view.findViewById(R.id.btn_rollback_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sale.delete();
                Item.deleteAll(Item.class, " sale= ? ", sale.getId()+"");
                getDialog().dismiss();
            }
        });
    }
}