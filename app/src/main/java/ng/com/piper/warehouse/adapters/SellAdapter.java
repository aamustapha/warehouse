package ng.com.piper.warehouse.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.customviews.lato.TextViewLato;
import ng.com.piper.warehouse.databases.Sold;

/**
 * Created by amustapha on 9/26/17.
 */

public class SellAdapter extends RecyclerView.Adapter<SellAdapter.ViewHolder> {

    List<Sold> items;
    long sale;

    public SellAdapter(List<Sold> items, long sale) {
        this.items = items;
        this.sale = sale;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sold, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.init(items.get(position));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextViewLato item;
        TextViewLato quantity, total;
        ImageView delete;

        public ViewHolder(View itemView) {
            super(itemView);

            item = itemView.findViewById(R.id.item_name);
            quantity = itemView.findViewById(R.id.item_quantity);
            total = itemView.findViewById(R.id.item_total);
            delete = (ImageView) itemView.findViewById(R.id.btn_delete_item);
        }

        public void init(final Sold sale){
            item.setText(sale.item.name);
            quantity.setText(""+sale.quantity);
            total.setText("N" + (sale.quantity * sale.item.price));

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    items.remove(sale);
                    sale.delete();
                    //todo: refresh recycler view
                }
            });
        }
    }
}
