package ng.com.piper.warehouse.databases;

import com.orm.SugarRecord;

/**
 * Created by amustapha on 9/26/17.
 */

public class Sold extends SugarRecord {
    public Sale sale;
    public Item item;
    public int quantity;

    public Sold(){}
    public Sold(Sale sale, Item item, int quantity) {
        this.sale = sale;
        this.item = item;
        this.quantity = quantity;
    }


}
