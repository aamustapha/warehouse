package ng.com.piper.warehouse.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.customviews.lato.TextViewLato;
import ng.com.piper.warehouse.databases.Sold;

/**
 * Created by amustapha on 9/26/17.
 */

public class SoldItemAdapter extends RecyclerView.Adapter<SoldItemAdapter.ViewHolder> {

    List<Sold> items;
    FragmentManager fm;

    public SoldItemAdapter(List<Sold> items, FragmentManager fm) {
        this.items = items;
        this.fm = fm;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sold2, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position == 0){
            holder.header();
        }else{
            holder.init(items.get(position -1));
        }


    }

    @Override
    public int getItemCount() {
        return items.size() +1;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextViewLato item, quantity, total;
        public ViewHolder(View itemView) {
            super(itemView);

            item = itemView.findViewById(R.id.item_name);
            quantity = itemView.findViewById(R.id.item_quantity);
            total = itemView.findViewById(R.id.item_total);
        }

        public void init(final Sold sale){
            item.setText(sale.item.name);
            quantity.setText(sale.quantity + "");
            total.setText("N" + (sale.quantity * sale.item.price));

        }

        public void header(){
            item.setText("Item");
            quantity.setText("Quantity");
            total.setText("Total");

            item.setGravity(View.TEXT_ALIGNMENT_CENTER);
            quantity.setGravity(View.TEXT_ALIGNMENT_CENTER);
            total.setGravity(View.TEXT_ALIGNMENT_CENTER);

        }

    }
}
