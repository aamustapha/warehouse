package ng.com.piper.warehouse.customviews.lato;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by one on 3/12/15.
 */
public class ButtonLato extends AppCompatButton {

    public ButtonLato(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonLato(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonLato(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
            setTypeface(tf);
        }
    }

}