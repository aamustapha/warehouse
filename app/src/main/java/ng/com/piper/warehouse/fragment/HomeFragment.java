package ng.com.piper.warehouse.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ng.com.piper.warehouse.HomeActivity;
import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.utilities.Constants;


public class HomeFragment extends Fragment implements View.OnClickListener {

    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(Object... vars) {

        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.sales).setOnClickListener(this);
        view.findViewById(R.id.store).setOnClickListener(this);
        view.findViewById(R.id.employees).setOnClickListener(this);
        view.findViewById(R.id.customers).setOnClickListener(this);
        view.findViewById(R.id.logout).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sales:
                switchTo(SalesFragment.newInstance());
                break;
            case R.id.store:
                switchTo(StoreFragment.newInstance());
                break;
            case R.id.employees:
                switchTo(StaffListFragment.newInstance());
                break;
            case R.id.customers:
                switchTo(CustomerListFragment.newInstance());
                break;
            case R.id.logout:
                switchTo(SigninFragment.newInstance(Constants.SET_PASSWORD));
                break;
        }
    }

    public void switchTo(Fragment fragment){
        getFragmentManager().beginTransaction()
                .replace(R.id.main_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void switchTo(Fragment fragment, boolean no){
        getFragmentManager().beginTransaction()
                .replace(R.id.main_container, fragment)
                .commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        ((HomeActivity) getActivity()).removeFab();
    }
}
