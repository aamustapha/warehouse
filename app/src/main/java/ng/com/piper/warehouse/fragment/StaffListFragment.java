package ng.com.piper.warehouse.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ng.com.piper.warehouse.HomeActivity;
import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.databases.Staff;


public class StaffListFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;

    public StaffListFragment() {
        // Required empty public constructor
    }

    public static StaffListFragment newInstance(Object... vars) {

        Bundle args = new Bundle();
        StaffListFragment fragment = new StaffListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView staffListView = view.findViewById(R.id.item_list);
        final List<Map<String, String>> staff_map_list = new ArrayList<>();
        for (Staff staff: Lists.newArrayList(Staff.findAll(Staff.class))){
            Map<String, String> m = new HashMap<>();
            m.put("name", staff.name);
            m.put("id", staff.getId() + "");
            m.put("username", staff.getRank());
            staff_map_list.add(m);
        }

        SimpleAdapter adapter = new SimpleAdapter(view.getContext(), staff_map_list, R.layout.row_staff, new String[]{"name", "username"}, new int[]{R.id.main_text, R.id.sub_text});
        staffListView.setAdapter(adapter);

        staffListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                StaffFragment frag = StaffFragment.newInstance(Long.parseLong(staff_map_list.get(i).get("id")));
                frag.show(getFragmentManager(), "");
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((HomeActivity) getActivity()).showFab(R.drawable.ic_employees, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaffFragment frag = StaffFragment.newInstance(0);
                frag.show(getFragmentManager(), "item");
            }
        });
    }
}
