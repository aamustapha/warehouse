package ng.com.piper.warehouse.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.databases.Item;
import ng.com.piper.warehouse.utilities.FormWrapper;


/**
 * Created by amustapha on 9/26/17.
 */

public class ItemFragment extends DialogFragment {
    public ItemFragment() {
//
    }
    private long id = 0;
    private Item item;
    FormWrapper wrapper;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item, container);
    }

    public static ItemFragment newInstance(long id) {
        ItemFragment frag = new ItemFragment();
        Bundle args = new Bundle();
        args.putLong("id", id);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        id = getArguments().getLong("id", 0);
        wrapper = new FormWrapper(view);
        init(view);

        view.findViewById(R.id.btn_commit_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                item.setName(wrapper.getEditTextValue(R.id.item_name))
                    .setAvailability(Integer.parseInt(wrapper.getEditTextValue(R.id.item_quantity)))
                    .setPrice(Integer.parseInt(wrapper.getEditTextValue(R.id.item_price)))
                    .save();
                Toast.makeText(getDialog().getContext(), "Item has been successfully added", Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }
        });

        view.findViewById(R.id.btn_delete_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(view.getContext())
                        .setTitle("Confirm delete")
                        .setIcon(R.drawable.ic_delete_sweep_black_24dp)
                        .setMessage("Are you sure you really want to delete this item? This action cannot be reversed")
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                item.delete();
                                Toast.makeText(getDialog().getOwnerActivity(), "Item has been successfully removed", Toast.LENGTH_SHORT).show();
                                getDialog().dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //abort
                            }
                        })
                        .show();



            }
        });

        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void init(View view){
        if (id > 0){
            item = Item.findById(Item.class, id);
            wrapper.injectEditText(R.id.item_name, item.name);
            wrapper.injectEditText(R.id.item_price, item.price +"");
            wrapper.injectEditText(R.id.item_quantity, item.availability +"");
        }else{
            item = new Item();
            view.findViewById(R.id.btn_delete_item).setVisibility(View.GONE);
        }
    }



}
