package ng.com.piper.warehouse.utilities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;

import ng.com.piper.warehouse.HomeActivity;
import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.fragment.SetPasswordFragment;

/**
 * Created by amustapha on 10/6/17.
 */

public class Constants {
    public static int SET_PASSWORD = 0;
    public static int HOME = 1;


    public static Runnable getAction(int target, final Context context, final FragmentManager fragMan){
        switch (target){
            case 0:
                return new Runnable() {
                    @Override
                    public void run() {
                        fragMan.beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left)
                                .replace(R.id.main_container, SetPasswordFragment.newInstance(HOME))
                                .commit();
                    }
                };
            case 1:
                return new Runnable() {
                    @Override
                    public void run() {
                            Intent i = new Intent(context, HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(i);
                    }
                };
            default:
                return new Runnable() {
                    @Override
                    public void run() {

                    }
                };
        }
    }
}
