package ng.com.piper.warehouse.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.databases.Sale;


public class CustomerListFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;

    public CustomerListFragment() {
        // Required empty public constructor
    }

    public static CustomerListFragment newInstance(Object... vars) {

        Bundle args = new Bundle();
        CustomerListFragment fragment = new CustomerListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView staffListView = view.findViewById(R.id.item_list);
        final List<Map<String, String>> staff_map_list = new ArrayList<>();
//        for (Sale sale: Sale.findWithQuery(Sale.class, " DISTINCT(phone), customer", null)){
        for (Sale sale: Lists.newArrayList(Sale.findAll(Sale.class))){
            Map<String, String> m = new HashMap<>();

            if(exists(staff_map_list, sale.phone))
                continue;
            m.put("name", sale.customer);
            m.put("username", sale.phone);
            m.put("id", sale.getId()+ "");
            staff_map_list.add(m);
        }


        SimpleAdapter adapter = new SimpleAdapter(view.getContext(), staff_map_list, R.layout.row_staff, new String[]{"name", "username"}, new int[]{R.id.main_text, R.id.sub_text});
        staffListView.setAdapter(adapter);

        staffListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View v, final int i, long l) {
               new AlertDialog.Builder(view.getContext())
                       .setTitle("Action")
                       .setMessage("What action would you like to perform on this customer?")
                       .setPositiveButton("Send message", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int ij) {
                               Sale s = Sale.findById(Sale.class, Long.parseLong(staff_map_list.get(i).get("id")));
                               String uri = "smsto:" + s.phone;
                               Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(uri));
                               intent.putExtra("compose_mode", true);
                               startActivity(intent);
                           }
                       })
                       .setNegativeButton("Call", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int ij) {
                               Sale s = Sale.findById(Sale.class, Long.parseLong(staff_map_list.get(i).get("id")));
                               Uri number = Uri.parse("tel:" + s.phone);
                               Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                               startActivity(callIntent);
                           }
                       })
                       .show();
            }
        });
    }

    public boolean exists(List<Map<String, String>> list,String phone){
        for (Map<String, String> m: list)
            if (m.get("phone").equals(phone))
                return true;

        return false;
    }
}
