package ng.com.piper.warehouse.utilities;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by amustapha on 9/26/17.
 */

public class FormWrapper {
    View view;

    public FormWrapper(View view) {
        this.view = view;
    }

    public String getEditTextValue(int id){
        return ((EditText) view.findViewById(id)).getText().toString();
    }

    public void injectEditText(int id, String value){
        ((EditText) view.findViewById(id)).setText(value);
    }

    public void disableEditText(int id){
        ((EditText) view.findViewById(id)).setEnabled(false);
    }

    public void injectTextView(int id, String value){
        ((TextView) view.findViewById(id)).setText(value);
    }


    public void clearEditText(int id){
        ((EditText) view.findViewById(id)).setText("");
    }


}
