package ng.com.piper.warehouse.customviews.lato;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by foram on 20/2/17.
 */

public class TextViewLatoBold extends AppCompatTextView {

    public TextViewLatoBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextViewLatoBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewLatoBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Bold.ttf");
            setTypeface(tf);
        }
    }

}
