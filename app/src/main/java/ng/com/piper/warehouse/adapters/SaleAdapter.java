package ng.com.piper.warehouse.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import ng.com.piper.warehouse.R;
import ng.com.piper.warehouse.customviews.lato.TextViewLato;
import ng.com.piper.warehouse.customviews.lato.TextViewLatoBold;
import ng.com.piper.warehouse.databases.Sale;
import ng.com.piper.warehouse.databases.Sold;
import ng.com.piper.warehouse.fragment.SaleFragment;

/**
 * Created by amustapha on 9/26/17.
 */

public class SaleAdapter extends RecyclerView.Adapter<SaleAdapter.ViewHolder> {

    List<Sale> items;
    FragmentManager fm;

    public SaleAdapter(List<Sale> items, FragmentManager fm) {
        this.items = items;
        this.fm = fm;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_sale, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.init(items.get(position), position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextViewLatoBold customer;
        TextViewLato total, date;
        ImageView delete;
        private long id;
        public ViewHolder(View itemView) {
            super(itemView);

            customer = itemView.findViewById(R.id.sale_customer);
            total = itemView.findViewById(R.id.sale_total);
            date = itemView.findViewById(R.id.sale_date);
            delete = (ImageView) itemView.findViewById(R.id.btn_delete_sale);


        }

        public void init(final Sale sale, final int position){
            customer.setText(sale.customer);
            date.setText(sale.getDate());

            total.setText("N" + compute(sale.getId()));
            id = sale.getId();

            customer.setOnClickListener(this);
            itemView.setOnClickListener(this);
            total.setOnClickListener(this);
            date.setOnClickListener(this);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sale.delete();
                    items.remove(sale);
                    notifyItemRemoved(position);
                }
            });

        }

        @Override
        public void onClick(View view) {
            SaleFragment details = SaleFragment.newInstance(id);
            details.show(fm, "tag");
        }

        public int compute(long id){
            int sum = 0;
            for(Sold s: Sold.find(Sold.class, " sale = ? ", String.valueOf(id))){
                sum += s.item.price * s.quantity;
            }
            return sum;
        }
    }
}
